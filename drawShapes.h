/**
*   This file provides some helper functions for drawing shape.
*
*   File: drawShapes.h
*   Author: Julian Carrivick
*   ID: 16164442
*   Unit: Computer Graphics 200
*   Date: November 2014
*   Curtin University
*/

#ifndef DRAWSHAPES_H
#define DRAWSHAPES_H

#include <GL/gl.h>
#include <GL/glut.h>

/* The drawing functions.
*   Does what it says on the box */

/*
*   drawGround
*   draws a polygon such that it looks like a plane, texturing it and creating
*   it at the appropiate height.
*/
void drawGround(double, GLuint, double);

/* 
*   drawSphere
*   Draws a sphere at the specified coordinates, colour and zoom factor.
*/
void drawSphere(double, double, double, GLfloat*, double);

/*
*   drawPolygon
*   Draws a polygon at the specified coordinates and zoom factory
*/
void drawTree(double, double, double, double);

/*
*   drawHouse
*   Draws a house at the specified Coordinates and wall roof and chimney
*   textures.
*/
void drawHouse(double, double, double, GLuint, GLuint, GLuint);

/*
*   drawText
*   Draws the given text on the screen given a location to write and the 
*   screen size
*/
void drawText(char*, double, double, double, double);

/*
*   drawCar
*   Draws a car at the given location with a zoom level and a distance 
*   travelled.
*/
void drawCar(double, double, double, double, double);



/* ******************************************
*           PRIVATE FUNCTIONS               *
*   These functions are only intended to be *
*       used in the drawShapes.c File       *
********************************************/
/*
*   drawCube
*   Given a size and texture, will draw a solid cube at the origin.
*   Code adapted from the glut source code at 
        https://www.opengl.org/resources/libraries/glut/
*/
void drawCube(GLfloat, GLuint);

/*
*   drawRoof
*   A helper function to draw the roof of the house given a height and texture
*/
void drawRoof(GLuint);

/*
*   drawChassis
*   Draws the chassis of the car
*/
void drawChassis(void);

/*
*   drawPanels
*   Draws the panels in between each side (eg the bonnet and rear window)
*   given a list of vertices to draw between
*/
void drawPanels(GLdouble [][3], int);

/*
*   drawWheel
*   Draws a wheel for the car given a zoom level and distance travelled
*/
void drawWheel(double, double);

/*
*   drawWindowAndDoor
*   Draws the windows and door on the sides of the car
*/
void drawWindowAndDoor(void);

#endif
