#ifndef BMPLOADER_H
#define BMPLOADER_H

/* Image type - contains height, width, and data */
typedef struct {
    unsigned long sizeX;
    unsigned long sizeY;
    char *data;
}Image;

int ImageLoad(char*, Image*);
#endif
