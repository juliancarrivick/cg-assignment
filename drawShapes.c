/**
*   This file provides some helper functions for drawing shape.
*
*   File: drawShapes.c
*   Author: Julian Carrivick
*   ID: 16164442
*   Unit: Computer Graphics 200
*   Date: November 2014
*   Curtin University
*/





#include <GL/gl.h>
#include <GL/glut.h>
#include <string.h>
#include <math.h>
#include "drawShapes.h"
#include "colours.h"
#include "glHelpers.h"




#define PI 3.14159265358979323846





/*
*   drawGround
*   draws a polygon such that it looks like a plane, texturing it and creating
*   it at the appropiate height.
*/
void drawGround(double z, GLuint texture, double size)
{
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, texture);
    glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, colour_white);

    glBegin(GL_QUADS);
        /* Draw a seperate polygon for each unit square of the ground so that
            lighting work properly and flat shading doesn't have as much of an
            effect */
        for (int i = 0; i < 2 * size; i++)
        {
            for (int j = 0; j < 2 * size; j++)
            {
                glNormal3d(0.0, 1.0, 0.0);
                glTexCoord2d(0.0, 0.0);
                glVertex3f(-size + i, z, -size + j);
                glTexCoord2d(1.0, 0.0);
                glVertex3f(-size + i + 1.0, z, -size + j);
                glTexCoord2d(1.0, 1.0);
                glVertex3f(-size + i + 1.0, z, -size + j + 1.0);
                glTexCoord2d(0.0, 1.0);
                glVertex3f(-size + i, z, -size + j + 1.0);
            }
        }
    glEnd();

    glDisable(GL_TEXTURE_2D);
}


/* 
*   drawSphere
*   Draws a sphere at the specified coordinates, colour and zoom factor.
*/
void drawSphere(double x, double y, double z, GLfloat* colour, double zoom)
{
    glPushMatrix();
        glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, colour);
        glTranslatef(x, y, z);
        glutSolidSphere(1.0, 10 * zoom, 8 * zoom);
    glPopMatrix();
}


/*
*   drawTree
*   Draws a tree at the specified coordinates
*/
void drawTree(double x, double y, double z, double zoom)
{
    GLUquadric *cylinder = gluNewQuadric();
    GLUquadric *cone = gluNewQuadric();

    glMaterialf(GL_FRONT, GL_SHININESS, 50.0);

    /* Do the tree trunk */
    glPushMatrix();
        glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, colour_brown);
        glTranslatef(x, y, z);
        glRotatef(-90, 1.0, 0.0, 0.0);
        gluCylinder(cylinder, 0.5, 0.5, 2.0, 10 * zoom, 8 * zoom);
    glPopMatrix();

    /* And the foliage at the top */
    glPushMatrix();
        glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, colour_green);
        glTranslatef(x, y, z);
        glTranslatef(0.0, 2.0, 0.0);
        glRotatef(-90, 1.0, 0.0, 0.0);
        gluCylinder(cone, 1.0, 0.0, 3.0, 10 * zoom, 8 * zoom);
    glPopMatrix();
}


/*
*   drawHouse
*   Draws a house at the specified Coordinates and zoom factor.
*/
void drawHouse(double x, double y, double z, GLuint wallTex, GLuint roofTex,
    GLuint chimneyTex)
{
    glMaterialf(GL_FRONT, GL_SHININESS, 128.0);
    glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, colour_white);
    
    glPushMatrix();
        glTranslatef(x, y, z);

        glPushMatrix();
            glTranslatef(0.0, 1.5, 0.0);
            glScaled(6.0, 3.0, 3.0);

            /* Create the main part of the house */
            drawCube(1.0, wallTex);

        glPopMatrix();

        /* Create the chimney */
        glPushMatrix();
            glTranslatef(-1.5, 5.0, 0.5);
            glScaled(1.0, 2.0, 1.0);
            drawCube(1.0, chimneyTex);
        glPopMatrix();

        /* Give the house a roof */
        drawRoof(roofTex);

        glBegin(GL_QUADS);
            /* Create the Door */
            glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, 
                colour_brown);      

            glNormal3d(0.0, 0.0, 1.0);
            glVertex3f(1.0, 0.0, 1.51);
            glVertex3f(2.0, 0.0, 1.51);
            glVertex3f(2.0, 2.0, 1.51);
            glVertex3f(1.0, 2.0, 1.51);

            /* And the door knob */
            glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, 
                colour_black);

            glVertex3f(1.85, 0.95, 1.52);
            glVertex3f(1.85, 1.05, 1.52);
            glVertex3f(1.95, 1.05, 1.52);
            glVertex3f(1.95, 0.95, 1.52);

            /* And the window */
            glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, 
                colour_white);

            glNormal3d(0.0, 0.0, 1.0);
            glVertex3f(-1.0, 1.5, 1.51);
            glVertex3f(-2.0, 1.5, 1.51);
            glVertex3f(-2.0, 2.5, 1.51);
            glVertex3f(-1.0, 2.5, 1.51);

            /* And the cross in the middle of the window */
            glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, 
                colour_red);

            glVertex3f(-2.0, 1.95, 1.52);
            glVertex3f(-2.0, 2.05, 1.52);
            glVertex3f(-1.0, 2.05, 1.52);
            glVertex3f(-1.0, 1.95, 1.52);

            glVertex3f(-1.55, 2.5, 1.52);
            glVertex3f(-1.45, 2.5, 1.52);
            glVertex3f(-1.45, 1.5, 1.52);
            glVertex3f(-1.55, 1.5, 1.52);
        glEnd();

    glPopMatrix();
}


/*
*   drawCar
*   Draws a car at the given location.
*/
void drawCar(double x, double y, double z, double zoom, double distance)
{
    glMaterialf(GL_FRONT, GL_SHININESS, 5.0);

    glPushMatrix();
        glTranslated(x, y + 0.25, z);
        glScaled(1.5, 1.5, 1.5);
    
        /* Draw the body of the car */
        drawChassis();

        /* And the wheels */
        glPushMatrix();
            glTranslated(0.625, 0.15, -1.0);
            drawWheel(zoom, distance);
            glTranslated(1.5, 0.0, 0.0);
            drawWheel(zoom, distance);
        glPopMatrix();

        /* Draw the windows and doors */
        drawWindowAndDoor();
        glTranslated(0.0, 0.0, -1.0);
        glScaled(1.0, 1.0, -1.0);
        drawWindowAndDoor();

    glPopMatrix();
}


/*
*   drawWindowAndDoor
*   Draws the windows and door on the sides of the car
*/
void drawWindowAndDoor(void)
{
    static int noVertices = 8;
    static GLdouble doorVertices[8][3] = 
        {
            {1.30, 0.1, 0.0},
            {1.30, 1.1, 0.0},
            {2.05, 1.1, 0.0},
            {2.05, 0.65, 0.0},
            {1.9, 0.65, 0.0},
            {1.6, 0.35, 0.0},
            {1.6, 0.1, 0.0},
            {1.30, 0.1, 0.0}
        };

    struct GLUtesselator *door = gluNewTess();

    /* Setup callback functions */
    gluTessCallback(door, GLU_TESS_BEGIN, (GLvoid (*) ())&tessBeginCall);
    gluTessCallback(door, GLU_TESS_END, (GLvoid (*) ())&tessEndCall);
    gluTessCallback(door, GLU_TESS_VERTEX, (GLvoid (*) ())&tessVertexCall);

    glPushMatrix();
        glTranslated(0.0, 0.0, 0.01);

        /* Back window */
        glBegin(GL_QUADS);
                glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, 
                    colour_black);
                glVertex3d(0.3, 0.65, 0.0);
                glVertex3d(0.3, 1.1, 0.0);
                glVertex3d(1.25, 1.1, 0.0);
                glVertex3d(1.25, 0.65, 0.0);
        glEnd();

        /* Door */
        glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, colour_grey);
        gluTessBeginPolygon(door, NULL);
            for (int i = 0; i < noVertices; i++)
            {
                gluTessVertex(door, doorVertices[i], doorVertices[i]);
            }
        gluTessEndPolygon(door);

        /* Door window */
        glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, colour_black);
        glBegin(GL_QUADS);
            glVertex3d(1.35, 0.7, 0.01);
            glVertex3d(1.35, 1.05, 0.01);
            glVertex3d(2.0, 1.05, 0.01);
            glVertex3d(2.0, 0.7, 0.01);
        glEnd();

        /* Door handle */
        glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, colour_blue);
        glBegin(GL_QUADS);
            glVertex3d(1.75, 0.55, 0.01);
            glVertex3d(1.85, 0.55, 0.01);
            glVertex3d(1.85, 0.65, 0.01);
            glVertex3d(1.75, 0.65, 0.01);
        glEnd();
    glPopMatrix();

    gluDeleteTess(door);
}


/*
*   drawChassis
*   Draws the chassis of the car
*/
void drawChassis(void)
{
    static int noVertices = 20;
    static GLdouble carVertices[20][3] = 
    {
        {0.0, 0.0, 0.0},
        {0.25, 0.0, 0.0},
        {0.25, 0.25, 0.0},
        {0.5, 0.5, 0.0},
        {0.75, 0.5, 0.0},
        {1.0, 0.25, 0.0},
        {1.0, 0.0, 0.0},
        {1.75, 0.0, 0.0},
        {1.75, 0.25, 0.0},
        {2.0, 0.5, 0.0},
        {2.25, 0.5, 0.0},
        {2.5, 0.25, 0.0},
        {2.5, 0.0, 0.0},
        {2.75, 0.0, 0.0},
        {2.75, 0.5, 0.0},
        {2.3, 0.75, 0.0},
        {2.0, 1.25, 0.0},
        {0.3, 1.25, 0.0},
        {0.0, 0.75, 0.0},
        {0.0, 0.0, 0.0}
    };

    struct GLUtesselator *car = gluNewTess();

    glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, colour_white);

    /* Setup call functions */
    gluTessCallback(car, GLU_TESS_BEGIN, (GLvoid (*) ())&tessBeginCall);
    gluTessCallback(car, GLU_TESS_END, (GLvoid (*) ())&tessEndCall);
    gluTessCallback(car, GLU_TESS_VERTEX, (GLvoid (*) ())&tessVertexCall);

    /* Do the front side of the car */
    glPushMatrix();
        glNormal3d(0.0, 0.0, 1.0);
        gluTessBeginPolygon(car, NULL);
            for (int i = 0; i < noVertices; i++)
            {
                gluTessVertex(car, carVertices[i], carVertices[i]);
            }
        gluTessEndPolygon(car);
    glPopMatrix();

    /* And the back side of the car */
    glPushMatrix();
        glTranslated(0.0, 0.0, -1.0);

        glNormal3d(0.0, 0.0, -1.0);
        gluTessBeginPolygon(car, NULL);
            for (int i = 0; i < noVertices; i++)
            {
                gluTessVertex(car, carVertices[i], carVertices[i]);
            }
        gluTessEndPolygon(car);
    glPopMatrix();

    /* Then join the gaps between each side */
    glPushMatrix();
        drawPanels(carVertices, noVertices);
    glPopMatrix();

    gluDeleteTess(car);
}


/*
*   drawWheel
*   Draws a wheel for the car
*/
void drawWheel(double zoom, double distance)
{
    GLUquadric *wheel = gluNewQuadric();
    GLUquadric *hubcap = gluNewQuadric();
        
    glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, colour_white);
    glMaterialf(GL_FRONT, GL_SHININESS, 70.0);

    double rotationInc = 360 * distance / 2 / PI / 0.3;

    /* Draw the main part of the wheel */
    glPushMatrix();
        glRotated(-rotationInc, 0.0, 0.0, 1.0);
        gluCylinder(wheel, 0.3, 0.3, 1.0, 10 * zoom, 8 * zoom);
    glPopMatrix();

    /* And each of the hubcaps */
    glPushMatrix();
        glTranslated(0.0, 0.0, 1.0);
        glRotated(-rotationInc, 0.0, 0.0, 1.0);
        gluCylinder(hubcap, 0.3, 0.0, 0.1, 10 * zoom, 8 * zoom);
    glPopMatrix();

    glPushMatrix();
        glTranslated(0.0, 0.0, -0.1);
        glRotated(-rotationInc, 0.0, 0.0, 1.0);
        gluCylinder(hubcap, 0.0, 0.3, 0.1, 10 * zoom, 8 * zoom);
    glPopMatrix();
}


/*
*   drawPanels
*   Draws the panels in between each side (eg the bonnet and rear window)
*   given a list of vertices to draw between
*/
void drawPanels(GLdouble carVertices[][3], int noVertices)
{
    static double split[6] = {0.0, 0.3, 0.45, 0.55, 0.7, 1.0};
    GLdouble tempV1[3], tempV2[3], tempV3[3], tempV4[3];

    glBegin(GL_QUADS);
        for (int i = 0; i < noVertices - 1; i++)
        {
            /* For the join across each side, this has been split to allow
                for the blue stripes */
            for (int j = 0; j < 5; j++)
            {
                /* Set the right colour depending on the part we are drawing 
                    */
                if (j == 1 || j == 3)
                {
                    glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, 
                        colour_blue);
                }
                else
                {
                    glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, 
                        colour_white);
                }

                /* However, if we are the windshield or the rear window, 
                    change back to tinted black */
                if (i == 15 || i == 17)
                {
                    glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, 
                        colour_black);
                }

                /* For the current and next car vertex, copy them into 
                    a temp vertex and alter the z component by the split
                    value  */
                for (int k = 0; k < 3; k++)
                {
                    tempV1[k] = carVertices[i][k];
                    tempV2[k] = carVertices[i+1][k];
                    tempV3[k] = carVertices[i][k];
                    tempV4[k] = carVertices[i+1][k];

                    if (k == 2)
                    {
                        /* Take from the z component */
                        tempV1[k] -= split[j];
                        tempV2[k] -= split[j];
                        tempV3[k] -= split[j+1];
                        tempV4[k] -= split[j+1];
                    }

                }

                /* Then find the normal for the polygon */
                applyNormal(tempV1, tempV2, tempV3);

                /* And lastly draw the polygon */
                glVertex3dv(tempV1);
                glVertex3dv(tempV2);
                glVertex3dv(tempV4);
                glVertex3dv(tempV3);
            }
        }
        
    glEnd();
}


/*
*   drawCube
*   Given a size and texture, will draw a solid cube at the origin.
*   Code adapted from the glut source code at 
        https://www.opengl.org/resources/libraries/glut/
    This was needed to apply textures to the cube.
*/
void drawCube(GLfloat size, GLuint texture)
{
    static GLfloat n[6][3] =
    {
        {-1.0, 0.0, 0.0},
        {0.0, 1.0, 0.0},
        {1.0, 0.0, 0.0},
        {0.0, -1.0, 0.0},
        {0.0, 0.0, 1.0},
        {0.0, 0.0, -1.0}
    };

    static GLint faces[6][4] =
    {
        {0, 1, 2, 3},
        {3, 2, 6, 7},
        {7, 6, 5, 4},
        {4, 5, 1, 0},
        {5, 6, 2, 1},
        {7, 4, 0, 3}
    };

    GLfloat v[8][3];
    GLint i;

    v[0][0] = v[1][0] = v[2][0] = v[3][0] = -size / 2;
    v[4][0] = v[5][0] = v[6][0] = v[7][0] = size / 2;
    v[0][1] = v[1][1] = v[4][1] = v[5][1] = -size / 2;
    v[2][1] = v[3][1] = v[6][1] = v[7][1] = size / 2;
    v[0][2] = v[3][2] = v[4][2] = v[7][2] = -size / 2;
    v[1][2] = v[2][2] = v[5][2] = v[6][2] = size / 2;

    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, texture);

    for (i = 5; i >= 0; i--) 
    {
        glBegin(GL_QUADS);
            glNormal3fv(&n[i][0]);

            glTexCoord2d(0.0, 0.0);         glVertex3fv(&v[faces[i][0]][0]);
            glTexCoord2d(0.0, size);        glVertex3fv(&v[faces[i][1]][0]);
            glTexCoord2d(size, size);       glVertex3fv(&v[faces[i][2]][0]);
            glTexCoord2d(size, 0.0);        glVertex3fv(&v[faces[i][3]][0]);
        glEnd();
    }
    glDisable(GL_TEXTURE_2D);
}


/*
*   drawRoof
*   A helper function to draw the roof of the house given a height and texture
*/
void drawRoof(GLuint texture)
{
    /* The roof vertices */
    static GLdouble roofV[][3] = 
        {
            {3.0, 3.0, 1.5},
            {3.0, 3.0, -1.5},
            {2.5, 5.0, 0},
            {-3.0, 3.0, 1.5},
            {-3.0, 3.0, -1.5},
            {-2.5, 5.0, 0},
            {-3.0, 3.0, 1.5},
            {3.0, 3.0, 1.5},
            {2.5, 5.0, 0},
            {-2.5, 5.0, 0},
            {-3.0, 3.0, -1.5},
            {3.0, 3.0, -1.5},
            {2.5, 5.0, 0},
            {-2.5, 5.0, 0}
        };

    /* Reset the colour */
    glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, colour_white);

    /* Bind the texture to the next objects */
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, texture);

    glBegin(GL_TRIANGLES);
        /* Do the right hand side of the roof */
        applyNormal(roofV[0], roofV[1], roofV[2]);

        glTexCoord2d(0.0, 0.0);     glVertex3dv(roofV[0]);
        glTexCoord2d(1.0, 0.0);     glVertex3dv(roofV[1]);
        glTexCoord2d(0.0, 1.0);     glVertex3dv(roofV[2]);

        /* And the left hand side */
        applyNormal(roofV[3], roofV[4], roofV[5]);

        glTexCoord2d(0.0, 0.0);     glVertex3dv(roofV[3]);
        glTexCoord2d(1.0, 0.0);     glVertex3dv(roofV[4]);
        glTexCoord2d(0.0, 1.0);     glVertex3dv(roofV[5]);
    glEnd();


    glBegin(GL_QUADS);
        /* Do the front part of the roof */
        applyNormal(roofV[6], roofV[7], roofV[8]);

        glTexCoord2d(0.0, 0.0);     glVertex3dv(roofV[6]);
        glTexCoord2d(6.0, 0.0);     glVertex3dv(roofV[7]);
        glTexCoord2d(6.0, 6.0);     glVertex3dv(roofV[8]);
        glTexCoord2d(0.0, 6.0);     glVertex3dv(roofV[9]);

        /* And the back part */
        applyNormal(roofV[10], roofV[11], roofV[12]);

        glTexCoord2d(0.0, 0.0);     glVertex3dv(roofV[10]);
        glTexCoord2d(6.0, 0.0);     glVertex3dv(roofV[11]);
        glTexCoord2d(6.0, 6.0);     glVertex3dv(roofV[12]);
        glTexCoord2d(0.0, 6.0);     glVertex3dv(roofV[13]);
    glEnd();

    glDisable(GL_TEXTURE_2D);
}


/*
*   drawText
*   Draws the given text on the screen
*/
void drawText(char *string, double x, double y, double width, double height)
{
    int len = strlen(string);
    void *font = GLUT_BITMAP_9_BY_15;

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluOrtho2D(0, width, height, 0);

    glRasterPos2i(x, y);

    for (int i = 0; i < len; i++)
    {
        glutBitmapCharacter(font, string[i]);
    }

    glMatrixMode(GL_MODELVIEW);
}
