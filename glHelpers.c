/**
*   This file provides some helper functions and utilities
*
*   File: glHelpers.c
*   Author: Julian Carrivick
*   ID: 16164442
*   Unit: Computer Graphics 200
*   Date: November 2014
*   Curtin University
*/




#include <GL/gl.h>
#include <GL/glut.h>
#include <math.h>




/*
*   applyNormal
*   Given three vertices, calculates the normal and applies it.
*   Function adapted from the code at:
*   http://www.gamedev.net/topic/310990-tell-opengl-to-calculate-normals-automatically/?view=findpost&p=2987532
*/
void applyNormal(double *v1, double *v2, double *v3)
{
    double vector1[3], vector2[3], crossP[3], normal[3];
    double length;

    /* Get two vectors to give the direction along the plane */
    for (int i = 0; i < 3; i++)
    {
        vector1[i] = v2[i] - v1[i];
        vector2[i] = v3[i] - v1[i];
    }
    
    /* Calculate the cross product */
    crossP[0] = vector1[1] * vector2[2] - vector1[2] * vector2[1];
    crossP[1] = vector1[2] * vector2[0] - vector1[0] * vector2[2];
    crossP[2] = vector1[0] * vector2[1] - vector1[1] * vector2[0];

    length = sqrt( crossP[0] * crossP[0] +
                   crossP[1] * crossP[1] + 
                   crossP[2] * crossP[2] );

    /* And normalise it */
    for (int i = 0; i < 3; i++)
    {
        normal[i] = crossP[i] / length;
    }

    /* Then set the normal value */
    glNormal3dv(normal);
}


/*
*   tessBeginCall
*   Callback function for tesselator begin
*/
void tessBeginCall(GLenum type)
{
    glBegin(type);
}


/*
*   tessEndCall
*   Callback function for tesselator end
*/
void tessEndCall(void)
{
    glEnd();
}


/*
*   tessVertexCall
*   Callback function for a vertex creation
*/
void tessVertexCall(GLvoid *vertex)
{
    glVertex3dv((GLdouble*)vertex);
}
