CC = gcc
CFLAGS = -Wall -std=c99 -pedantic
LINKERFLAGS = -lglut -lGLU -lGL -lm
OBJ = outback.o BMP_Loader.o drawShapes.o linked_list.o glHelpers.o

all: outback

outback: $(OBJ)
	$(CC) $(CFLAGS) -o outback $(OBJ) $(LINKERFLAGS)

outback.o: outback.c outback.h colours.h drawShapes.h BMP_Loader.h
	$(CC) $(CFLAGS) -c outback.c

BMP_Loader.o: BMP_Loader.c BMP_Loader.h
	$(CC) $(CFLAGS) -c BMP_Loader.c

drawShapes.o: drawShapes.c drawShapes.h colours.h
	$(CC) $(CFLAGS) -c drawShapes.c

linked_list.o: linked_list.c linked_list.h
	$(CC) $(CFLAGS) -c linked_list.c

glHelpers.o: glHelpers.c glHelpers.h
	$(CC) $(CFLAGS) -c glHelpers.c

clean:
	rm -rf $(OBJ) outback
	