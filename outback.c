/**
*   This program renders a scene in OpenGL.
*
*   File: outback.c
*   Author: Julian Carrivick
*   ID: 16164442
*   Unit: Computer Graphics 200
*   Date: November 2014
*   Curtin University
*/




/* Include standard libraries */
#include <stdio.h>
#include <math.h>
#include <stdlib.h>

/* And those for OpenGL/GLUT */
#include <GL/gl.h>
#include <GL/glut.h>

/* The Bitmap loader */
#include "BMP_Loader.h"

/* And my header files */
#include "outback.h"
#include "colours.h"
#include "drawShapes.h"
#include "linked_list.h"



#define PI 3.14159265358979323846




/*
*   main
*   Sets up the window and gets rendering!
*/
int main(int argc, char **argv)
{
    /* Setup the window */
    glutInit(&argc, argv);
    glutInitDisplayMode( GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH );
    glutInitWindowSize(WINDOW_WIDTH, WINDOW_HEIGHT);
    glutInitWindowPosition(100, 100);

    /* Create the window and initialise! */
    windowID = glutCreateWindow("CG200 Outback Assignment");
    init();

    /* Set the callback functions */
    glutIdleFunc(idle);
    glutDisplayFunc(display);
    glutReshapeFunc(reshape);
    glutKeyboardFunc(input);

    /* And start her up! */
    glutMainLoop();

    return 0;
}


/*
*   init
*   Sets up the scene by initialising(!) everything
*/
void init(void)
{
    /* Set the background colour, enable depth testing and smooth 
        shading */
    glClearColor(0.53, 0.804, 0.98, 0.0);
    glEnable(GL_DEPTH_TEST);
    glShadeModel(GL_SMOOTH);

    /* Enable Transperancy */
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    /* Setup lighting */
    GLfloat mat_specular[] = {1.0, 1.0, 1.0, 1.0};
    GLfloat mat_shininess[] = {50.0};
    GLfloat ambCol[] = {0.2, 0.2, 0.2, 1.0};
    GLfloat lightColour1[] = {1.0, 1.0, 1.0, 1.0};
    GLfloat lightColour2[] = {0.5, 0.5, 0.5, 1.0};
    GLfloat light_position[] = {0.0, 5.0, 0.0, 1.0};

    glLightModelfv(GL_LIGHT_MODEL_AMBIENT, ambCol);

    glMaterialfv(GL_FRONT, GL_SPECULAR, mat_specular);
    glMaterialfv(GL_FRONT, GL_SHININESS, mat_shininess);

    glLightfv(GL_LIGHT0, GL_DIFFUSE, lightColour1);
    glLightfv(GL_LIGHT0, GL_POSITION, light_position);

    glLightfv(GL_LIGHT1, GL_POSITION, light_position);
    glLightfv(GL_LIGHT1, GL_DIFFUSE, lightColour2);

    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
    glEnable(GL_LIGHT1);

    glEnable(GL_NORMALIZE);

    /* Initialise Globals */
    for (int i = 0; i < 2; ++i)
    {
        /* Although we use an int here, these structs are intended to be 
            accessed by the enum Axis values ie axisRotation[Y_AXIS] */
        axisRotation[i].rotationValue = 0.0;
        axisRotation[i].rotationIncrement = 0;
    }

    /* Generate textures and add Texture names to Linked List*/
    LinkedList *textureNames = createLinkedList();
    insertBack(textureNames, (void*)("Textures/Grass.bmp"));
    insertBack(textureNames, (void*)("Textures/whitebrick.bmp"));
    insertBack(textureNames, (void*)("Textures/tiles.bmp"));
    insertBack(textureNames, (void*)("Textures/redbrick.bmp"));
    insertBack(textureNames, (void*)("Textures/sand.bmp"));

    loadTextures(textureNames);

    while (textureNames->count > 0)
    {
        removeBack(textureNames);
    }

    free(textureNames);

    zoomLevel = 1.0;

    /* Enable Fog */
    glFogi(GL_FOG_MODE, GL_EXP);
    glFogfv(GL_FOG_COLOR, colour_white);
    glFogf(GL_FOG_DENSITY, 0.03);
    glHint(GL_FOG_HINT, GL_FASTEST);
    glFogf(GL_FOG_START, 5.0);
    glFogf(GL_FOG_END, 30.0);
    glEnable(GL_FOG);
}


/* 
*   idle
*   Describes what the system should do when idle - in this case redisplay
*   everything
*/
void idle(void)
{
    glutPostRedisplay();
}


/*
*   display
*   Displays everything
*/
void display(void)
{
    static GLfloat pos1[] = {0.0, 10.0, -18.0, 1.0};
    static GLfloat pos2[] = {0.0, 10.0, -5.0, 1.0};
    static int noHelpStrings = 14;
    static char *helpStrings[] = 
        {
            "Welcome to Julian's OpenGL Program.",
            "Press 'Z' to zoom in and 'z' to zoom out.",
            "Press 'X' or 'x' to rotate around the X Axis.",
            "Press 'Y' or 'y' to rotate around the Y Axis.",
            "Press 'A' or 'a' to start the animation.",
            "Press 'F' or 'f' to speed up the animation.",
            "Press 'S' or 's' to slow down the animation.",
            "Press 'T' or 't' to pause the animation",
            "Press 'C' or 'c' to resume animation",
            "Press 'p' to switch to flat polygon rendering.",
            "Press 'P' to switch to smooth polygon rendering.",
            "Press 'H' or 'h' to bring this menu back up.",
            "Press Ctrl-Esc to close the program.",
            "Press any other key to close this menu."
        };

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
    clockVal += clockInc;

    /* Have to disable lights and fog so the text colour doesn't get
    *   altered */
    if (showHelp)
    {
        /* For some reason cant do this: */
        // glDisable(GL_FOG | GL_LIGHT0 | GL_LIGHT1 );
        glDisable(GL_FOG);
        glDisable(GL_LIGHT0);
        glDisable(GL_LIGHT1);
        drawHelp(helpStrings, noHelpStrings);
        glEnable(GL_FOG);
        glEnable(GL_LIGHT0);
        glEnable(GL_LIGHT1);
    }

    glPushMatrix();
        rotateSceneAroundAxis(X_AXIS);
        rotateSceneAroundAxis(Y_AXIS);

        /* Translate the entire scene to normalise the coordinates
            since the viewport bottom clipping plane is at y = -6.0
            and the front clipping plne is at z = -10.0 */
        glTranslatef(0.0, -6.0, -10.0);

        /* Setup the sun */
        glPushMatrix();
            glRotated(clockVal / 5, 0.0, 0.0, 1.0);
            glMaterialf(GL_FRONT, GL_SHININESS, 128.0);
            glLightfv(GL_LIGHT0, GL_POSITION, pos1);
            drawSphere(0.0, 10.0, -20.0, (GLfloat*)colour_yellow, zoomLevel);

            /* If the sun has rotated between 90 and 270 degrees, then it is 
                night time */
            if (90 < (int)clockVal / 5 % 360  && (int)clockVal / 5 % 360 < 270 )
            {
                /* Then turn off the light and set the background to black as 
                    well as reducing the ambient light */
                glDisable(GL_LIGHT0);
                glClearColor(0.0, 0.0, 0.0, 0.0);
                GLfloat ambCol[] = {0.01, 0.01, 0.01, 1.0};
                glLightModelfv(GL_LIGHT_MODEL_AMBIENT, ambCol);
            }
            else
            {
                /* Set to initial values */
                glEnable(GL_LIGHT0);
                glClearColor(0.53, 0.804, 0.98, 0.0);
                GLfloat ambCol[] = {0.2, 0.2, 0.2, 1.0};
                glLightModelfv(GL_LIGHT_MODEL_AMBIENT, ambCol);

            }
        glPopMatrix();

        /* And the moon */
        glPushMatrix();
            glTranslated(0.0, 0.0, -8.0);
            glRotated(-clockVal / 2 + 180.0, 0.0, 1.0, 0.0);
            glMaterialf(GL_FRONT, GL_SHININESS, 128.0);
            glLightfv(GL_LIGHT1, GL_POSITION, pos2);
            drawSphere(0.0, 10.0, -5.0, (GLfloat*)colour_grey, zoomLevel);
        glPopMatrix();

        /* The desert floor */
        drawGround(0.0, textures[4], SCENE_INFINITY);
        
        /* The grass garden */
        glPushMatrix();
            glTranslatef(0.0, 0.0, -8.0);
            drawGround(0.01, textures[0], 6.0);
        glPopMatrix();

        /* My pine trees */
        drawTree(5.0, 0.0, -10.0, zoomLevel);
        drawTree(-5.0, 0.0, -10.0, zoomLevel);
        drawTree(4.5, 0.0, -5.0, zoomLevel);
        drawTree(-4.5, 0.0, -11.0, zoomLevel);

        /* Home sweet home */
        drawHouse(0.0, 0.0, -12.0, textures[3], textures[2], textures[1]);

        /* And I can't forget my wheels */
        glPushMatrix();
            glTranslatef(0.0, 0.0, -12.0);
            glRotatef(clockVal, 0.0, 1.0, 0.0);
            glTranslatef(0.0, 0.0, 8.0);
            drawCar(0.0, 0.0, 0.0, zoomLevel, clockVal/360.0*2*PI*8.0);
        glPopMatrix();

    glPopMatrix();

    glFlush();
    glutSwapBuffers();
}


/*
*   reshape
*   Sets up the projection transformation
*/
void reshape(int width, int height)
{
    glViewport(0, 0, (GLsizei)width, (GLsizei)height);

    glMatrixMode(GL_PROJECTION);

    glLoadIdentity();
    glFrustum(viewPort.leftClip, viewPort.rightClip, viewPort.bottomClip, 
                viewPort.topClip, viewPort.nearClip, viewPort.farClip);

    glMatrixMode(GL_MODELVIEW);
}


/*
*   rotateSceneAroundX
*   Provides the logic to rotate the entire scene around the x axis
*/
void rotateSceneAroundAxis(Axis axis)
{
    axisRotation[axis].rotationValue += 
                            (double)axisRotation[axis].rotationIncrement / 5;

    if (axisRotation[axis].rotationValue > 360.0)
        axisRotation[axis].rotationValue -= 360.0;

    switch(axis)
    {
        case X_AXIS:
            glRotatef(axisRotation[axis].rotationValue, 1.0, 0.0, 0.0);
            break;
        case Y_AXIS:
            glRotatef(axisRotation[axis].rotationValue, 0.0, 1.0, 0.0);
            break;
        case Z_AXIS:
            glRotatef(axisRotation[axis].rotationValue, 0.0, 0.0, 1.0);
            break;
        default:
            ; // Do nothing
    }
}


/*
*   input
*   Handles what happens when a key is pressed
*/
void input(unsigned char key, int x, int y)
{   
    static double tempClockInc = 0.0;

    switch (key)
    {
        case 'Z':
            zoomIn();                                           
            break;
        case 'z':
            zoomOut();                                          
            break;

        case 'Y': case 'y':
            axisRotation[Y_AXIS].rotationIncrement = 
                1 - axisRotation[Y_AXIS].rotationIncrement;     
            break;

        case 'X': case 'x':
            axisRotation[X_AXIS].rotationIncrement = 
                1 - axisRotation[X_AXIS].rotationIncrement;     
            break;

        case 'A': case 'a':
            if (!animationStarted)
            {
                clockInc = 0.01;
                animationStarted = TRUE;
            }                       
            break;

        case 'F': case 'f':
            if (animationStarted && clockInc < 1.0)
            {
                clockInc += 0.01;
            }                       
            break;

        case 'S': case 's':
            if (animationStarted && clockInc > 0.0)
            {
                clockInc -= 0.01;
            }                       
            break;

        case 'T': case 't':
            if (animationStarted && fabs(tempClockInc) < 0.0001)
            {
                tempClockInc = clockInc;
                clockInc = 0.0;
            }
            break;

        case 'C': case 'c':
            if (animationStarted && fabs(tempClockInc) > 0.0001)
            {
                clockInc = tempClockInc;
                tempClockInc = 0.0;
            }
            break; 

        case 'p':
            glShadeModel(GL_FLAT);                              
            break;

        case 'P':
            glShadeModel(GL_SMOOTH);                            
            break;

        case 'H': case 'h':
            showHelp = TRUE;                                    
            break;

        case 27: /* Escape Key */
            if (glutGetModifiers() == GLUT_ACTIVE_CTRL) /* If Ctrl-Esc */
            {
                /* Close the program */
                glutDestroyWindow(windowID);
                exit(0);
            }
            break;

        default:
            if (showHelp)
            {
                showHelp = FALSE;
            }
    }
}


/*
*   zoomIn
*   Zooms in until a predifined limit is hit
*/
void zoomIn(void)
{
    double viewWidth = viewPort.rightClip - viewPort.leftClip;

    if (viewWidth > 0.5)
    {
        zoom(scaleIncrement);
    }
}

/*
*   zoomOut
*   Zooms out until a predfined limit is hit.
*/
void zoomOut(void)
{
    double viewWidth = viewPort.rightClip - viewPort.leftClip;

    if (viewWidth < 7.0)
    {
        zoom(-scaleIncrement);
    }
}

/*
*   zoom
*   Changes the viewport size by the speicifed increment
*/
void zoom(double incrementAmount)
{
    double viewWidth = viewPort.rightClip - viewPort.leftClip;

    viewPort.leftClip += incrementAmount;
    viewPort.rightClip -= incrementAmount;
    viewPort.bottomClip += incrementAmount;
    viewPort.topClip -= incrementAmount;
    viewPort.nearClip += incrementAmount;
    viewPort.farClip += incrementAmount;

    reshape(WINDOW_WIDTH, WINDOW_HEIGHT);

    zoomLevel = 6.0 / viewWidth;
}

/*
*   drawHelp
*   Draws each of the strings in the array of strings given
*/
void drawHelp(char **strings, int noStrings)
{
    glMaterialf(GL_FRONT, GL_SHININESS, 100.0);

    for (int i = 0; i < noStrings; i++)
    {
        drawText(strings[i], 10, 14*(i+1), WINDOW_WIDTH, WINDOW_HEIGHT);
        
    }
    reshape(WINDOW_WIDTH, WINDOW_HEIGHT);
}


/* 
*   loadTextures
*   Loads the linked list of texture names into memory
*/
void loadTextures(LinkedList *textureNames)
{
    glEnable(GL_TEXTURE_2D);

    textures = (GLuint*)malloc(textureNames->count * sizeof(GLuint));
    glGenTextures(textureNames->count, textures);

    /* Load each of the textures */
    for (int i = 1; i <= textureNames->count; ++i)
    {
        Image tempBitmap;
        
        if (ImageLoad((char*)peekIthElement(textureNames, i), &tempBitmap))
        {
            glBindTexture(GL_TEXTURE_2D, textures[i-1]);

            /* Setup linear interpolation and to repeat the texture */
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
            glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);

            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, tempBitmap.sizeX, 
                tempBitmap.sizeY, 0, GL_RGB, GL_UNSIGNED_BYTE, 
                tempBitmap.data);
        }
    }
    glDisable(GL_TEXTURE_2D);
}
