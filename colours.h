/**
*   This file specifies colours for fast reference
*
*   File: colours.h
*   Author: Julian Carrivick
*   ID: 16164442
*   Unit: Computer Graphics 200
*   Date: November 2014
*   Curtin University
*/




#ifndef COLOURS_H
#define COLOURS_H

static const GLfloat colour_red[]       = {1.00, 0.00, 0.00, 1.00};
static const GLfloat colour_green[]     = {0.00, 1.00, 0.00, 1.00};
static const GLfloat colour_blue[]      = {0.00, 0.00, 1.00, 1.00};
static const GLfloat colour_yellow[]    = {1.00, 1.00, 0.00, 1.00};
static const GLfloat colour_purple[]    = {1.00, 0.00, 1.00, 1.00};
static const GLfloat colour_white[]     = {1.00, 1.00, 1.00, 1.00};
static const GLfloat colour_black[]     = {0.00, 0.00, 0.00, 1.00};
static const GLfloat colour_brown[]     = {0.63, 0.32, 0.18, 1.00};
static const GLfloat colour_clear[]     = {1.0, 1.0, 1.0, 0.0};
static const GLfloat colour_grey[]      = {0.50, 0.50, 0.50, 0.50};

#endif
