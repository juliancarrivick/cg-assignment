/**
*   linked_list provides functions and structs that are to be used as a linked
*   list. Any linked list or its node are all allocated on the heap.
*
*   File: linked_list.h
*   Author: Julian Carrivick
*   ID: 16164442
*   Unit: Computer Graphics 200
*   Date: November 2014
*   Curtin University
*/




#ifndef LINKEDLIST_H
#define LINKEDLIST_H




/****************
*   DATA TYPES  *
****************/

/**
*   A struct which is the node on a linked list. To allow any data to be
*   associated it has a void pointer. It is doubly linked to allow backwards
*   traversal.
*       data - a void pointer to be able to point to anything.
*       next - a pointer to the next node in the Linked List
*       prev - a pointer to the previous node in the Linked List
*/
typedef struct Node
{
    void *data;
    struct Node *next, *prev;
} Node;


/**
*   The Linked List struct. Also contains a count field for quicj checking
*   of the size of the LinkedList. It is double ended, allowing quick access
*   to the last element.
*       count - the number of elements in the list.
*       head - a pointer to the head of the list.
*       tail - a pointer to the tail of the list.
*/
typedef struct
{
    int count;
    Node *head, *tail;
} LinkedList;




/****************
*   FUNCTIONS   *
****************/

/**
*   createLinkedList
*       Purpose: To create a new, empty linked list for generic use.
*       Inport: None
*       Export: A pointer to the newly created linked list.
*/
LinkedList* createLinkedList();


/**
*   insertFront
*   Purpose: To insert a value into the front of a linked list.
*   Import: A linked list pointer, the list to add to.
*           A void pointer, the data to add to the linked list.
*   Export: None.
*/
void insertFront( LinkedList*, void* );


/**
*   insertBack
*   Purpose: To insert a value into the back of a linked list.
*   Import: A linked list pointer, the list to add to.
*           A void pointer, the data to add to the linked list.
*   Export: None.
*/
void insertBack( LinkedList*, void* );


/**
*   insertIthElement
*   Purpose: To insert a value into an arbitary point in a linked list.
*   Import: A linked list pointer, the list to add to.
*           A void pointer, the data to add to the linked list.
*           An int, the point at which the data will be at after being inserted.
                for example, 1 will mean it is the first element.
*   Export: None.
*   Comments: This function cannot insert into an empty list.
*/
void insertIthElement( LinkedList*, void*, int );


/**
*   peekFront
*   Purpose: To return the value of the first item in the list.
*   Import: A Linked List pointer, the list to get from.
*   Export: A void pointer, the data of the head node.
*/
void* peekFront( LinkedList* );


/**
*   peekBack
*   Purpose: To return the value of the last item in the list.
*   Import: A Linked List pointer, the list to get from.
*   Export: A void pointer, the data of the head node.
*/
void* peekBack( LinkedList* );


/**
*   peekIthElement
*   Purpose: To return the value of the ith element in the linked list. 
*       If the ith element is beyond the length of the linked list, NULL is 
*       returned.
*   Import: A LinkedList pointer, the list to retrieve from
*           An int, the element to retrieve.
*   Export: A void pointer, the data of the ith element.    
*/
void* peekIthElement( LinkedList*, int );


/**
*   removeFront
*   Purpose: Removes the first item in the linked list and returns its value. Also
*       frees the removed nodes memory.
*   Import: A LinkedList pointer, the list to remove from.
*   Export: A Void pointer, the value of the old front node.
*   Comments: If there are no items in the list, then NULL is returned.
*/
void* removeFront( LinkedList* );


/**
*   removeBack
*   Purpose: To removes tha last item in the linked list and returns its value. 
*       Also frees the removed node's memory.
*   Import: A LinkedList pointer, the list to remove from.
*   Export: A Void pointer, the value of the old back node.
*   Comments: If there are no items in the list, then NULL is returned.
*/
void* removeBack( LinkedList* );


/**
*   removeIthElement
*   Purpose: To remove the Ith element from a linked list. 
*   Import: A LinkedList pointer, the list to retrieve from
*           An int, the element to retrieve.
*   Export: A void pointer, the data of the ith element.
*   Comments: If there are no items in the list, or the location supplied is
*       invalid, then NULL is returned.
*/
void *removeIthElement( LinkedList*, int );


/**
*   isEmpty
*   Purpose: To determin whether the linked list is empty or not.
*   Import: A LinkedList Pointer
*   Export: boolean, specifies whether the list is empty or not.
*/
int isEmpty( LinkedList* );

#endif
