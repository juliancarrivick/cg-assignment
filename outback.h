/**
*   This file provides function headers for the file outback.c
*
*   File: outback.h
*   Author: Julian Carrivick
*   ID: 16164442
*   Unit: Computer Graphics 200
*   Date: November 2014
*   Curtin University
*/

#ifndef OUTBACK_H
#define OUTBACK_H

#include "BMP_Loader.h"
#include "linked_list.h"

#define FALSE 0
#define TRUE !FALSE




/* Program Datatypes */

/*
*   RotationAxis
*   Describes which axis will be rotated around 
*/
typedef enum
{
    X_AXIS = 0, Y_AXIS, Z_AXIS
} Axis;

/*  Rotation
*   Describes an arbitary rotation value around an axis.
*/
typedef struct
{
    double rotationValue;
    int rotationIncrement;
} Rotation;

/*  ViewPort
*   Descibes the viewport in terms of the parameters in glFrustrum
*/
typedef struct
{
    double leftClip, rightClip, bottomClip, topClip, nearClip, farClip;
} ViewPort;




/* Program constants */
#define WINDOW_WIDTH 600
#define WINDOW_HEIGHT 600
#define SCENE_INFINITY 200.0

static const double scaleIncrement = 0.1;



/* Global Variables */
static int windowID;
static Rotation axisRotation[3];
static GLuint* textures;
static ViewPort viewPort = {-3.0, 3.0, -3.0, 3.0, 5.0, 30.0};
static double zoomLevel = 1;

static double clockVal = 0.0;
static double clockInc = 0.00;
static int animationStarted = FALSE;

static int showHelp = TRUE;





/* Function Declarations */

/* These are the callback functions which glut calls when the described action
    occurs */
void init(void);
void idle(void);
void display(void);
void input(unsigned char, int, int);
void reshape(int, int);

/* Utility functions */
void rotateSceneAroundAxis(Axis);
void zoomIn(void);
void zoomOut(void);
void zoom(double);
void drawHelp(char**, int);
void loadTextures(LinkedList*);

#endif
