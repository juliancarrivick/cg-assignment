/**
*   This file provides some helper functions and utilities
*
*   File: glHelpers.h
*   Author: Julian Carrivick
*   ID: 16164442
*   Unit: Computer Graphics 200
*   Date: November 2014
*   Curtin University
*/



#ifndef HELPER_H
#define HELPER_H

/*
*   applyNormal
*   Given three points (in a 3x3 matrix), calculates the normal and applies
*   it
*/
void applyNormal(double*, double*, double*);

/* Callback frunctions for the Tesselator */
void tessBeginCall(GLenum);
void tessEndCall(void);
void tessVertexCall(GLvoid*);

#endif
