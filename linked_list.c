/**
*   linked_list provides functions and structs that are to be used as a linked
*   list. Any linked list or its node are all allocated on the heap.
*
*   File: linked_list.c
*   Author: Julian Carrivick
*   ID: 16164442
*   Unit: Computer Graphics 200
*   Date: November 2014
*   Curtin University
*/




#include <stdlib.h>
#include <stdio.h>
#include "linked_list.h"




/**
*   createLinkedList
*       Purpose: To create a new, empty linked list for generic use.
*       Inport: None
*       Export: A pointer to the newly create linked list.
*/
LinkedList *createLinkedList()
{
    LinkedList *list;

    list = (LinkedList*)malloc( sizeof( LinkedList ) );
    list->head = NULL;
    list->tail = NULL;
    list->count = 0;

    return list;
}


/**
*   insertFront
*   Purpose: To insert a value into the front of a linked list.
*   Import: A linked list pointer, the list to add to.
*           A void pointer, the data to add to the linked list.
*   Export: None.
*/
void insertFront( LinkedList* list, void* data )
{
    Node *newNode;

    newNode = (Node*)malloc( sizeof( Node ) );
    newNode->data = data;
    newNode->prev = NULL;

    if ( list->count == 0 )
        list->tail = newNode;
    else
        list->head->prev = newNode;

        
    list->count++;
    newNode->next = list->head;
    list->head = newNode;

}


/**
*   insertBack
*   Purpose: To insert a value into the back of a linked list.
*   Import: A linked list pointer, the list to add to.
*           A void pointer, the data to add to the linked list.
*   Export: None.
*/
void insertBack( LinkedList* list, void* data )
{
    Node *newNode;

    newNode = (Node*)malloc( sizeof( Node ) );
    newNode->data = data;
    newNode->next = NULL;

    if ( list->count == 0 )
        list->head = newNode;
    else
    {
        list->tail->next = newNode;
        newNode->prev = list->tail;
    }

    list->tail = newNode;
    list->count++;
}


/**
*   insertIthElement
*   Purpose: To insert a value into an arbitary point in a linked list.
*   Import: A linked list pointer, the list to add to.
*           A void pointer, the data to add to the linked list.
*           An int, the point at which the data will be at after being inserted.
                for example, 1 will mean it is the first element.
*   Export: None.
*   Comments: This function cannot insert into an empty list.
*/
void insertIthElement( LinkedList* list, void *data, int location )
{
    Node *newNode, *currNode;
    int i;

    newNode = (Node*)malloc( sizeof( Node ) );
    newNode->data = data;

    if ( location == 1 )
        insertFront( list, data );
    else if ( ( 1 < location ) && ( location <= list->count ) )
    {
        currNode = list->head;

        for ( i = 1; i < location; i++ )
            currNode = currNode->next;

        /*  Making the neccassary pointer adjustments to insert the new node. */
        newNode->prev = currNode->prev;
        newNode->next = currNode;
        currNode->prev->next = newNode;
        currNode->prev = newNode;

        list->count++;
    }
    else
        fprintf( stderr, "Location Index is not valid.\n" );

}


/**
*   peekFront
*   Purpose: To return the value of the first item in the list.
*   Import: A Linked List pointer, the list to get from.
*   Export: A void pointer, the data of the head node.
*/
void *peekFront( LinkedList* list )
{
    return list->head->data;
}


/**
*   peekBack
*   Purpose: To return the value of the last item in the list.
*   Import: A Linked List pointer, the list to get from.
*   Export: A void pointer, the data of the head node.
*/
void *peekBack( LinkedList* list )
{
    return list->tail->data;
}


/**
*   peekIthElement
*   Purpose: To return the value of the ith element in the linked list. 
*       If the ith element is beyond the length of the linked list, NULL is 
*       returned.
*   Import: A LinkedList pointer, the list to retrieve from
*           An int, the element to retrieve.
*   Export: A void pointer, the data of the ith element.    
*/
void *peekIthElement( LinkedList* list, int element )
{
    int i;
    void* data;
    Node *currNode = NULL;

    if ( list->count >= element )
    {
        currNode = list->head;
        /*  Finding the right node */
        for ( i = 1; i < element; i++ )
            currNode = currNode->next;
    }
    else
    {
        fprintf(stderr, "inElement is larger than count.\n");
    }

    if ( currNode == NULL )
        data = NULL;
    else
        data = currNode->data;

    return data;
}


/**
*   removeFront
*   Purpose: Removes the first item in the linked list and returns its value. Also
*       frees the removed nodes memory.
*   Import: A LinkedList pointer, the list to remove from.
*   Export: A Void pointer, the value of the old front node.
*   Comments: If there are no items in the list, then NULL is returned.
*/
void *removeFront( LinkedList* list )
{
    void* data = NULL;
    Node* oldNode;

    if ( list->count > 0 )
    {
        data = list->head->data;
        oldNode = list->head;

        if ( list->count == 1 )
        {
            list->head = NULL;
            list->tail = NULL;
        }
        else
        {
            list->head = list->head->next;
            list->head->prev = NULL;
        }

        list->count--;
        free( oldNode );
    }

    return data;    
}


/**
*   removeBack
*   Purpose: To removes the last item in the linked list and returns its value. 
*       Also frees the removed node's memory.
*   Import: A LinkedList pointer, the list to remove from.
*   Export: A Void pointer, the value of the old back node.
*   Comments: If there are no items in the list, then NULL is returned.
*/
void *removeBack( LinkedList* list )
{
    void* data = NULL;
    Node* oldNode;

    if ( list->count > 0 )
    {
        data = list->tail->data;
        oldNode = list->tail;

        if ( list->count == 1 )
        {
            list->head = NULL;
            list->tail = NULL;
        }
        else
        {
            list->tail = list->tail->prev;
            list->tail->next = NULL;
        }

        list->count--;
        free( oldNode );
    }

    return data;
}


/**
*   removeIthElement
*   Purpose: To remove the Ith element from a linked list. 
*   Import: A LinkedList pointer, the list to retrieve from
*           An int, the element to retrieve.
*   Export: A void pointer, the data of the ith element.
*   Comments: If there are no items in the list, or the location supplied is
*       invalid, then NULL is returned.
*/
void *removeIthElement( LinkedList *list, int location )
{
    int i;
    Node *currNode;
    void *data = NULL;

    if ( list->count > 0 )
    {
        if ( location == 1 )
            data = removeFront( list );
        else if ( location == list->count )
            data = removeBack( list );
        else if ( ( 1 < location ) && ( location < list->count ) )
        {
            currNode = list->head;

            for ( i = 1; i < location; i++ )
            {
                currNode = currNode->next;
            }

            currNode->prev->next = currNode->next;
            currNode->next->prev = currNode->prev;
            data = currNode->data;

            list->count--;
            free( currNode );
        }
    }

    return data;
}


/**
*   isEmpty
*   Purpose: To determin whether the linked list is empty or not.
*   Import: A LinkedList Pointer
*   Export: boolean, specifies whether the list is empty or not.
*/
int isEmpty( LinkedList* list )
{
    return list->count == 0;
}
